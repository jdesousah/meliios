//
//  StringExtension.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import Foundation

extension String {
    public func textOrEmptyCharacter() -> String {
        return !self.isEmpty ? self : "-"
    }
}
