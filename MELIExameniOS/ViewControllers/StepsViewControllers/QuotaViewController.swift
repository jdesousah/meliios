//
//  QuotaViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class QuotaViewController: UIViewController {
    
    private let unwindSegueIdentifier: String = "unwindProcessCompletedSegue"
    
    @IBOutlet weak var quotaPickerView: UIPickerView!
    
    @IBOutlet weak var quotaSelectedLabel: UILabel!
    
    var sharedViewModel: PaymentViewModel = PaymentViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sharedViewModel = self.stepsController.results.value(forKey: "sharedViewModel") as! PaymentViewModel
        let selectedPayment = self.stepsController.results.value(forKey: "paymentMethod") as! String
        let selectedAmount: Double = self.stepsController.results.value(forKey: "amount") as! Double
        let selectedIssuer = self.stepsController.results.value(forKey: "selectedBank") as! String
        
        CircularSpinner.show("Consultando información...", animated: true,type: .indeterminate, showDismissButton:false)
        sharedViewModel.retrieveInstallment(amount: selectedAmount, paymentMethod: selectedPayment, issuer: selectedIssuer, success: { (success) in
            self.quotaPickerView.reloadAllComponents()
            self.quotaPickerView.selectRow(0, inComponent: 0, animated: false)
            self.pickerView(self.quotaPickerView, didSelectRow: 0, inComponent: 0)
            //self.quotaSelectedLabel.text = self.sharedViewModel.installment!.payerCosts[0].recommendedMessage
            CircularSpinner.hide()
        }) { (serverError) in
            CircularSpinner.hide()
            self.showMessage(title: "Ha ocurrido un error", message: serverError.domain)
        }
    }

    @IBAction func nextStepAction() {
        self.stepsController.showNextStep()
    }
    
    @IBAction func previousStepAction() {
        self.stepsController.showPreviousStep()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == unwindSegueIdentifier {
            (segue.destination as! MainViewController).data = self.stepsController.results
        }
    }
}

extension QuotaViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        quotaSelectedLabel.text = sharedViewModel.installment!.payerCosts[row].recommendedMessage
    }
}

extension QuotaViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sharedViewModel.installment?.payerCosts.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sharedViewModel.installment!.payerCosts[row].recommendedMessage
    }
}
