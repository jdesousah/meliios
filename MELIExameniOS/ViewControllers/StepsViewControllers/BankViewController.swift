//
//  BankViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class BankViewController: UIViewController {
    
    @IBOutlet weak var disabledBlurEffect: UIVisualEffectView!
    
    @IBOutlet weak var previousButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var selectorView: SimpleSelectionView!
    
    var sharedViewModel: PaymentViewModel = PaymentViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PaymentViewController.selection))
        gestureRecognizer.delegate = self
        selectorView.addGestureRecognizer(gestureRecognizer)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sharedViewModel = self.stepsController.results.value(forKey: "sharedViewModel") as! PaymentViewModel
        let selectedPayment = self.stepsController.results.value(forKey: "paymentMethod") as! String
        
        CircularSpinner.show("Consultando información...", animated: true,type: .indeterminate, showDismissButton:false)
        sharedViewModel.retrieveIssuers(paymentMethod: selectedPayment, success: { (success) in
            CircularSpinner.hide()
            
            if self.sharedViewModel.issuers.count == 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.stepsController.results.setValue("", forKey: "selectedBank")
                    self.nextStepAction()
                })
                
            }
        }) { (serverError) in
            self.showMessage(title: "Importante", message: serverError.domain)
        }
        self.disabledBlurEffect.isHidden = self.nextButton.isEnabled//TODO: KVO
    }
    
    @IBAction func nextStepAction() {
        self.stepsController.showNextStep()
    }
    
    @IBAction func previousStepAction() {
        self.stepsController.showPreviousStep()
    }
}

extension BankViewController: UIGestureRecognizerDelegate {
    @objc func selection() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let selectionViewController = storyboard.instantiateViewController(withIdentifier: "simpleSelectionViewController") as! SimpleItemSelectionViewController
        selectionViewController.delegate = self
        selectionViewController.selectionModel = self.sharedViewModel.issuers.map({ (issuer) in
            return SimpleSelectionVO(id: issuer.id, imageURL: issuer.secureThumbnail, name: issuer.name)
        })
        self.present(selectionViewController, animated: true, completion: nil)
    }
}

extension BankViewController: SimpleOptionSelectionProtocol {
    func didSelectItem(item: SimpleSelectionVO) {
        self.selectorView.titleLabel.text = item.name
        self.selectorView.sourceImageView.imageFromUrl(urlString: item.imageURL)
        self.stepsController.results.setValue(item.id, forKey: "selectedBank")
    }
}
