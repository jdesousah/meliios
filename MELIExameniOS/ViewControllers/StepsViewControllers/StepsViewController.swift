//
//  StepsViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit
import RMStepsController

class StepsViewController: RMStepsController {
    var localStepsViewControllersArray: [Any]? = nil
    var viewModel: PaymentViewModel = PaymentViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.stepsBar.hideCancelButton = true
        
        viewModel = PaymentViewModel()
        
        self.results.setValue(viewModel, forKey: "sharedViewModel")
    }

    override func stepViewControllers() -> [Any]! {
        if localStepsViewControllersArray != nil {
            return localStepsViewControllersArray
        }
        
        let storyboard = UIStoryboard(name: "Steps", bundle: nil)
        
        let amountStep = storyboard.instantiateViewController(withIdentifier: "amountViewController")
        amountStep.step.title = "Monto a pagar"
        
        let paymentStep = storyboard.instantiateViewController(withIdentifier: "paymentViewController")
        paymentStep.step.title = "Medio de pago"
        
        let bankStep = storyboard.instantiateViewController(withIdentifier: "bankViewController")
        bankStep.step.title = "Selección de Banco"
        
        let quotaStep = storyboard.instantiateViewController(withIdentifier: "quotaViewController")
        quotaStep.step.title = "Cuotas"
        
        localStepsViewControllersArray = [amountStep, paymentStep, bankStep, quotaStep]
        
        return localStepsViewControllersArray
    }
    
    override func finishedAllSteps() {
        //NOTE: All data fullfilled step by step can be stored inside "results" dictionary
        print(self.results)
        
        (self.stepViewControllers().last as! QuotaViewController).performSegue(withIdentifier: "unwindProcessCompletedSegue", sender: self)
        
        localStepsViewControllersArray = nil
    }
    
    override func canceled() {
        self.navigationController?.popViewController(animated: true)
    }

}
