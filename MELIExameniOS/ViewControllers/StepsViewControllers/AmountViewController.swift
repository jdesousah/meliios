//
//  AmountViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class AmountViewController: UIViewController {
    
    @IBOutlet weak var disabledBlurEffect: UIVisualEffectView!
    
    fileprivate let maxAmountDigits: Int = 10
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var amountTextField: UITextField!
    
    @IBOutlet weak var bottomButtonsConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector:#selector(AmountViewController.keyboardWillAppear(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(AmountViewController.keyboardWillDisappear(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        amountTextField.delegate = self
        self.nextButton.isEnabled = false
        self.disabledBlurEffect.isHidden = false
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bottomButtonsConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    @IBAction func nextStepAction() {
        self.stepsController.results.setValue(Double(amountTextField.text!), forKey: "amount")
        self.stepsController.showNextStep()
    }
    
    @IBAction func previousStepAction() {
        self.stepsController.showPreviousStep()
    }
    
    //NOTE: The nexts two methods exists just for move buttons from botton when keyboard is showed/hidden
    @objc func keyboardWillAppear(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            self.animateButtons(keyboardHeight)
        }
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.animateButtons(-keyboardHeight)
        }
    }
    
    private func animateButtons(_ distance: CGFloat) {
        bottomButtonsConstraint.constant = distance
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

extension AmountViewController: UITextFieldDelegate {    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text, let validValue = Int(text + string) {
            self.nextButton.isEnabled = validValue > 0
            self.disabledBlurEffect.isHidden = self.nextButton.isEnabled//TODO: KVO
            let charCount = text.characters.count + string.characters.count - range.length
            return isInsideCharLimits(charCount: charCount)
        }
        else {
            self.nextButton.isEnabled = false
            self.disabledBlurEffect.isHidden = self.nextButton.isEnabled//TODO: KVO
            return isInsideCharLimits(charCount: maxAmountDigits + 1)
        }
    }
    
    //para mantener la longitud del texto dentro de los limites deseados
    private func isInsideCharLimits(charCount: Int) -> Bool {
        return charCount <= maxAmountDigits
    }
}
