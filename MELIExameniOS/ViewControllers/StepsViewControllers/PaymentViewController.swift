//
//  PaymentViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

//import CircularSpinner

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var disabledBlurEffect: UIVisualEffectView!//TODO: Llevar a protocol dsde el base
    
    
    @IBOutlet weak var selectorView: SimpleSelectionView!
    
    @IBOutlet weak var previousButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    var sharedViewModel: PaymentViewModel = PaymentViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PaymentViewController.selection))
        gestureRecognizer.delegate = self
        selectorView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sharedViewModel = self.stepsController.results.value(forKey: "sharedViewModel") as! PaymentViewModel
        CircularSpinner.show("Consultando información...", animated: true,type: .indeterminate, showDismissButton:false)
        sharedViewModel.retrievePaymentMethods(success: { (success) in
            CircularSpinner.hide()
        }) { (serverError) in
            CircularSpinner.hide()
            self.showMessage(title: "Importante", message: serverError.domain)
        }
        
        nextButton.isEnabled = self.stepsController.results.value(forKey: "paymentMethod") != nil
        self.disabledBlurEffect.isHidden = self.nextButton.isEnabled//TODO: KVO
    }
    
    @IBAction func nextStepAction() {
        self.stepsController.showNextStep()
    }
    
    @IBAction func previousStepAction() {
        self.stepsController.showPreviousStep()
    }

}

extension PaymentViewController: UIGestureRecognizerDelegate {
    @objc func selection() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let selectionViewController = storyboard.instantiateViewController(withIdentifier: "simpleSelectionViewController") as! SimpleItemSelectionViewController
        selectionViewController.delegate = self
        selectionViewController.selectionModel = self.sharedViewModel.payments.map({ (payment) in
            return SimpleSelectionVO(id: payment.id, imageURL: payment.secureThumbnail, name: payment.name)
        })
        self.present(selectionViewController, animated: true, completion: nil)
    }
}

extension PaymentViewController: SimpleOptionSelectionProtocol {
    func didSelectItem(item: SimpleSelectionVO) {
        self.selectorView.titleLabel.text = item.name
        self.selectorView.sourceImageView.imageFromUrl(urlString: item.imageURL)
        self.stepsController.results.setValue(item.id, forKey: "paymentMethod")
    }
}
