//
//  SimpleItemSelectionViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class SimpleItemSelectionViewController: UIViewController {
    
    static let cellIdentifier: String = "simpleTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectionModel: [SimpleSelectionVO] = [SimpleSelectionVO]()
    var delegate: SimpleOptionSelectionProtocol?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "SimpleTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: SimpleItemSelectionViewController.cellIdentifier)
    }
}

extension SimpleItemSelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectItem(item: selectionModel[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}

extension SimpleItemSelectionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectionModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentModel = selectionModel[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SimpleItemSelectionViewController.cellIdentifier) as! SimpleTableViewCell
        cell.titleLabel.text = currentModel.name
        cell.customImageView.imageFromUrl(urlString: currentModel.imageURL)
        
        return cell
    }
}
