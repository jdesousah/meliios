//
//  MainViewController.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    public var data: NSMutableDictionary = NSMutableDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func startAction() {
        
    }
    
    @IBAction func unwindToMenu(_ segue: UIStoryboardSegue) {
        
        print(data)
        
        self.navigationController?.popToRootViewController(animated: true)
        //TODO: Mostrar la selección en un alerta segun ejer.
        self.showMessage(title: "Resultado", message: formattedMessage())
    }
    
    private func formattedMessage() -> String {
        var formattedText = "Tu selección fue:"
        
        formattedText += String(format: "\n Monto: $%.02f", data.value(forKey: "amount") as! Double)
        formattedText += String(format: "\n Forma de pago: \(data.value(forKey: "paymentMethod") as! String)")
        formattedText += "\n Banco: " + (data.value(forKey: "selectedBank") as! String).textOrEmptyCharacter()
        
        return formattedText
    }
}
