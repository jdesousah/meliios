//
//  SimpleOptionSelectionProtocol.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import Foundation

protocol SimpleOptionSelectionProtocol {
    func didSelectItem(item: SimpleSelectionVO) -> Void
}
