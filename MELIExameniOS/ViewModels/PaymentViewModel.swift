//
//  PaymentViewModel.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class PaymentViewModel: NSObject {
    
    let dataSource: MPPaymentDataSource
    var payments: [Payment] = [Payment]()
    var issuers: [CardIssuer] = [CardIssuer]()
    var installment: Installment?
    
    override init() {
        dataSource = MPPaymentDataSource()
    }
    
    func retrievePaymentMethods(success: @escaping(BaseBooleanSuccessBlock), failure: @escaping(BaseErrorBlock)) {
        dataSource.requestPaymentMethods(success: { (payments) in
            self.payments = payments
            success(true)
        }) { (serverError) in
            failure(serverError)
        }
    }
    
    func retrieveIssuers(paymentMethod: String, success: @escaping(BaseBooleanSuccessBlock), failure: @escaping(BaseErrorBlock)) {
        dataSource.requestCardIssuers(paymentMethod: paymentMethod, success: { (issuers) in
            self.issuers = issuers
            success(true)
        }) { (serverError) in
            failure(serverError)
        }
    }
    
    func retrieveInstallment(amount: Double, paymentMethod: String, issuer: String, success: @escaping(BaseBooleanSuccessBlock), failure: @escaping(BaseErrorBlock)) {
        dataSource.requestInstallment(amount: amount, paymentMethod: paymentMethod, issuer: issuer, success: { (installmentSuccess) in
            self.installment = installmentSuccess
            success(true)
        }) { (serverError) in
            failure(serverError)
        }
    }

}
