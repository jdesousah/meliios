//
//  SimpleSelectionVO.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class SimpleSelectionVO: NSObject {
    let id: String
    let imageURL: String
    let name: String
    
    init(id: String, imageURL: String, name: String) {
        self.id = id
        self.imageURL = imageURL
        self.name = name
    }
}
