//
//  CardIssuer.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//


import SwiftyJSON

class CardIssuer: NSObject {

    let id: String
    let name: String
    let thumbnail: String
    let processingMode: String
    let secureThumbnail: String
    let merchantAccountId: Any?
    
    init?(withJSON json: JSON) {
        guard let _ = json["id"].string else{ return nil }
        guard let _ = json["name"].string else{ return nil }
        guard let _ = json["secure_thumbnail"].string else{ return nil }
        guard let _ = json["thumbnail"].string else{ return nil }
        guard let _ = json["processing_mode"].string else{ return nil }
        
        id = json["id"].stringValue
        name = json["name"].stringValue
        secureThumbnail = json["secure_thumbnail"].stringValue
        thumbnail = json["thumbnail"].stringValue
        processingMode = json["processing_mode"].stringValue
        merchantAccountId = json["merchant_account_id"]
    }
}
