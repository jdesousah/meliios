//
//  Installment.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import SwiftyJSON

class Installment: NSObject {
    
    let paymentMethodId: String
    var payerCosts: [PayerCost] = [PayerCost]()
    
    init?(withJSON json: JSON) {
        
        guard let _ = json["payment_method_id"].string else { return nil }
        guard let _ = json["payer_costs"].array else { return nil }
        
        paymentMethodId = json["payment_method_id"].stringValue
        
        payerCosts = [PayerCost]()
        for item in json["payer_costs"].arrayValue {
            if let validPayerItem = PayerCost(fromJSON: item) {
                payerCosts.append(validPayerItem)
            }
        }
    }
}
