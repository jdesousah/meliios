//
//  PayerCost.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import SwiftyJSON

class PayerCost: NSObject {
    
    let installment: Int
    let installmentRate: Int
    let recommendedMessage: String
    
    init?(fromJSON json: JSON) {
        guard let _ = json["installments"].int else { return nil }
        guard let _ = json["installment_rate"].int else { return nil }
        guard let _ = json["recommended_message"].string else { return nil }
        
        installment = json["installment"].intValue
        installmentRate = json["installment_rate"].intValue
        recommendedMessage = json["recommended_message"].stringValue
    }
}
