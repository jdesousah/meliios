//
//  PaymentModel.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit
import SwiftyJSON

class Payment: NSObject {
    let id: String
    let name: String
    let paymentTypeId: String
    let status: String
    let secureThumbnail: String
    let thumbnail: String
    let deferredCapture: String
//    let settings: [Any]
//    let additionalInfoNeeded: [Any]
    let minAllowedAmount: Int
    let maxAllowedAmount: Int
    let accreditationTime: Int
//    let financialInstitutions: [Any]
    let processingModes: [String]
    
    init?(withJSON json: JSON) {
        //by default i consider this object as invalid if at least one value is not fullfilled from service
        guard let _ = json["id"].string else { return nil }
        guard let _ = json["name"].string else { return nil }
        guard let _ = json["payment_type_id"].string else { return nil }
        guard let _ = json["status"].string else { return nil }
        guard let _ = json["secure_thumbnail"].string else { return nil }
        guard let _ = json["thumbnail"].string else { return nil }
        guard let _ = json["deferred_capture"].string else { return nil }
        guard let _ = json["min_allowed_amount"].int else { return nil }
        guard let _ = json["max_allowed_amount"].int else { return nil }
        guard let _ = json["accreditation_time"].int else { return nil }
        guard let _ = json["processing_modes"].array else { return nil }
        
        
        id = json["id"].stringValue
        name = json["name"].stringValue
        paymentTypeId = json["payment_type_id"].stringValue
        status = json["status"].stringValue
        secureThumbnail = json["secure_thumbnail"].stringValue
        thumbnail = json["thumbnail"].stringValue
        deferredCapture = json["deferred_capture"].stringValue
        minAllowedAmount = json["min_allowed_amount"].intValue
        maxAllowedAmount = json["max_allowed_amount"].intValue
        accreditationTime = json["accreditation_time"].intValue
        processingModes = json["processing_modes"].arrayValue.map { $0.stringValue }
    }
}
