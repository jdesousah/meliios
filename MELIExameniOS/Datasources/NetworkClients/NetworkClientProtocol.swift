//
//  NetworkClientAFNetworking.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

//It enables to switch between (as eg.) AFNetworking with other libraries
protocol NetworkClientProtocol {
    
    func get(path: String, params: [String: Any]?, success: @escaping(BaseSuccessBlock), failure: @escaping(BaseErrorBlock))
    
    func post(path: String, params: [Any]?, success: @escaping BaseSuccessBlock, failure: @escaping(BaseErrorBlock))
    
    func put(path: String, params: [Any], success: BaseSuccessBlock, failure: BaseErrorBlock)
    
    func delete(path: String, params: [Any], success: BaseSuccessBlock, failure: BaseErrorBlock)
}
