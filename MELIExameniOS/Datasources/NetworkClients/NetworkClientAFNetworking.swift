//
//  NetworkClientAFNetworking.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import SwiftyJSON
import AFNetworking

class NetworkClientAFNetworking: NetworkClientProtocol {
    
    let manager = AFHTTPSessionManager()
    
    func get(path: String, params: [String: Any]?, success: @escaping(BaseSuccessBlock), failure: @escaping(BaseErrorBlock)) {
//        manager.requestSerializer.setValue(.getCurrendDeviceID(), forHTTPHeaderField: "deviceid")
        manager.get(path, parameters: params, progress: nil, success: { (sessiondatatask, responseObject) in
            print("GET: success " + responseObject.debugDescription)
            success(JSON(responseObject as Any))
            
        }) { (datatask, serverError) in
            failure(NSError(domain: "Ha ocurrido un error. Intenta nuevamente en unos segundos.", code: 10999, userInfo: nil))
        }
    }
    
    func post(path: String, params: [Any]?, success: @escaping BaseSuccessBlock, failure: @escaping(BaseErrorBlock)) {
        
        manager.post(path, parameters: params, success: { (dataTask, responseObject) in
            print("POST: success " + responseObject.debugDescription)
            success(JSON(responseObject as Any))
        }) { (sessiondatatask, serverError) in
            failure(serverError as NSError)
        }
    }
    
    func put(path: String, params: [Any], success: BaseSuccessBlock, failure: BaseErrorBlock) {
        manager.put(path, parameters: params, success: { (sessiontask, responseObject) in
            //NOTE: Future implementations!
        }) { (sessiondatatask, serverError) in
            //NOTE: Future implementations!
        }
    }
    
    func delete(path: String, params: [Any], success: BaseSuccessBlock, failure: BaseErrorBlock) {
        manager.delete(path, parameters: params, success: { (sessiontask, responseObject) in
            //NOTE: Future implementations!
        }) { (sessiondatatask, serverError) in
            //NOTE: Future implementations!
        }
    }
}
