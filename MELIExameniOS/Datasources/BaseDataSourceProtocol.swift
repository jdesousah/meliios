//
//  BaseDataSourceProtocol.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias BaseBooleanSuccessBlock = (Bool) -> Void
typealias BaseSuccessBlock = (JSON) -> Void
typealias BaseErrorBlock = (NSError) -> Void

protocol BaseDataSourceProtocol {
    var baseURL: String { get set }
    var networkClient: NetworkClientProtocol { get set }
}
