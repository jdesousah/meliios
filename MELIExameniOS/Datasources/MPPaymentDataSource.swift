//
//  MPPaymentDataSource.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

typealias PaymentMethodsSuccessBlock = ([Payment]) -> Void
typealias CardIssuerSuccessBlock = ([CardIssuer]) -> Void
typealias InstallmentSuccessBlock = (Installment) -> Void

class MPPaymentDataSource: NSObject, BaseDataSourceProtocol {
    
    var baseURL: String
    var networkClient: NetworkClientProtocol
    
    override init() {
        networkClient = NetworkClientAFNetworking()
        baseURL = "https://api.mercadopago.com/v1/payment_methods"
    }
    
    func requestPaymentMethods(success: @escaping(PaymentMethodsSuccessBlock), failure: @escaping(BaseErrorBlock)) -> Void {
        
        let parameters = ["public_key" : getMPApiKey()]
        
        networkClient.get(path: baseURL, params: parameters, success: { (successJSON) in
            
            if let secureJSON = successJSON.array {
                var methods: [Payment] = [Payment]()
                for paymentMethod in secureJSON {
                    if let validModel = Payment(withJSON: paymentMethod) {
                        methods.append(validModel)
                    }
                }
                success(methods)
            }
            else {
                failure(NSError(domain: "Los datos retornados del WS no son validos.", code: 999, userInfo: nil))
            }
            
        }) { (serverError) in
            failure(serverError)
        }
    }
    
    func requestCardIssuers(paymentMethod: String, success: @escaping(CardIssuerSuccessBlock), failure: @escaping(BaseErrorBlock)) -> Void {
        let fullpath = baseURL + "/card_issuers"
        let parameters = ["public_key" : getMPApiKey(),
                          "payment_method_id": paymentMethod]
        
        networkClient.get(path: fullpath, params: parameters, success: { (successJSON) in
            if let secureJSON = successJSON.array {
                var cardIssuers: [CardIssuer] = [CardIssuer]()
                for issuer in secureJSON {
                    if let validIssuer = CardIssuer(withJSON: issuer) {
                        cardIssuers.append(validIssuer)
                    }
                }
                success(cardIssuers)
            }
            else {
                failure(NSError(domain: "Los datos retornados del WS no son validos.", code: 998, userInfo: nil))
            }
        }) { (serverError) in
            failure(serverError)
        }
    }
    
    func requestInstallment(amount:Double, paymentMethod: String, issuer: String, success: @escaping(InstallmentSuccessBlock), failure: @escaping(BaseErrorBlock)) -> Void {
        
        let fullPath = baseURL + "/installments"
        let parameters: [String : Any] = ["public_key" : getMPApiKey(),
                          "amount": amount,
                          "payment_method_id": paymentMethod,
                          "issuer.id": issuer]
        networkClient.get(path: fullPath, params: parameters, success: { (successJSON) in
            
            if let installmentItem = successJSON.array {
                if  let validInstallment = Installment(withJSON: installmentItem.first!) {
                    success(validInstallment)
                }
                else {
                    failure(NSError(domain: "Los datos retornados del WS no son validos.", code: 997, userInfo: nil))
                }
            }
            else {
                failure(NSError(domain: "Los datos retornados del WS no son validos.", code: 997, userInfo: nil))
            }
            
        }) { (serverError) in
            failure(serverError)
        }
    }
    
    fileprivate func getMPApiKey() -> String {
        var apiKey: String = ""
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            apiKey = dict["MELIMPPublicKey"] as! String
        }
        return apiKey
    }
}
