//
//  SimpleTableViewCell.swift
//  MELIExameniOS
//
//  Copyright © 2018 Joaquin de Sousa. All rights reserved.
//

import UIKit

class SimpleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var customImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
